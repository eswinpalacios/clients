package com.eswinpalacios.clients.utils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public abstract class ClientValidation2 {
    public static boolean validate (Date dateOfBirthday, int age) throws Exception {
        Date dateNow = new Date();
        long diffTime = dateNow.getTime() - dateOfBirthday.getTime();
        int ageDaysNow = (int) TimeUnit.DAYS.convert(diffTime, TimeUnit.MILLISECONDS);
        int ageYear = ageDaysNow / 365;

        if(ageYear>=age)
            throw new Exception("Bad Request");
        return false;
    }
}
